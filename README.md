# bupgui

bupgui is a primitive GUI for the commandline backup tool bup.
It does not support any settings, and thus cannot be used in a wrong way by inexperienced users.

It saves the home folder of the user executing it.



How to install and configure:

\#!/bin/bash

sudo apt install python3-tk fuse3 bup

cd /media/backupdrive

git clone https://salsa.debian.org/mess42/bupgui.git

cd bupgui

mkdir bupdir

mkdir mountpoint

bup -d bupdir init

python3 bupgui.py
